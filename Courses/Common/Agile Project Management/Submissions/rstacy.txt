(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Ryan Stacy
(Course Site): Lynda
(Course Name): Agile Project Management
(Course URL): http://www.lynda.com/Business-Project-Management-tutorials/Agile-Project-Management/122428-2.html
(Discipline): SDLC
(ENDIGNORE)

(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some of the benefits Agile has over a Waterfall methodology? 
(A): The ability to conform to shifting business needs
(B): The ability to have remote employees
(C): Gaining business value earlier
(D): You can focus on a delivery date
(Correct): A, C
(Points): 1
(CF): Agile is flexible with business needs and will get more immediate feedback.
(WF): Remote employees are always possible, but Agile focuses on face-to-face communication.  Delivery dates may shift in an Agile environment.  
(STARTIGNORE)
(Hint): Technology used does not make something agile
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): How many times do you go through the Envision stage? 
(A): Once
(B): At the beginning of every sprint
(C): Every time business needs change
(D): Every day
(Correct): A
(Points): 1
(CF): 
(WF): The Envision stage only happens once.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which items are to be considered at the Speculate phase? 
(A): New items
(B): Backlog items
(C): Incomplete items from previous sprint
(D): Overflow items from other teams
(Correct): A, B, C
(Points): 1
(CF): 
(WF): There should not be overflow from other teams.  Agile teams should be working independently from one another.
(STARTIGNORE)
(Hint): Agile teams are independent
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Why does the first iteration of the speculate phase take longer?
(A): Because you are estimating all the features in the product
(B): Because it is likely no one has done Agile before
(C): Because you should always estimate bigger on the first try
(D): Because you will have more features to do in the first sprint
(Correct): A
(Points): 1
(CF): 
(WF): On the first iteration, none of the features are estimated.  This means that all of them need to be estimated before you can move on to the next phase.
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): True or False. Iterations are exclusive to Agile processes.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Correct.  Just because something has iterations doesn't mean it is Agile.
(WF): Agile is not the only patter to use iterations.  
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What are some things to consider when prioritizing features?
(A): Whether or not the team has worked together in Agile before
(B): The risk of the features
(C): The experience of other teams on the project
(D): The collaborative technology used
(Correct): A, B
(Points): 1
(CF): 
(WF): Agile teams should be independent. The collaborative technology should not influence feature priority. 
(STARTIGNORE)
(Hint): Technology used is usually not a factor
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): Who looks at future sprints to confirm proper business alignment?
(A): Project Manager
(B): Development team
(C): Scrum master
(D): Business Analyst
(Correct): D
(Points): 1
(CF): 
(WF): The business analyst is best suited to see misalignment with the business.
(STARTIGNORE)
(Hint): The core team should not be looking far ahead
(Subject): SDLC
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 0
(Random answers): 1
(Question): What are some good methods to add energy to daily stand ups?
(A): Change the order of who gives their status
(B): Keep the team standing during the meeting
(C): Have the meeting at a different time everyday
(D): Resolve issues as soon as they are mentioned
(Correct):  A, B
(Points): 1
(CF): 
(WF): Consistency is key to these meetings.  The meeting should not be interrupted to resolve issues.  This needs to happen after.
(STARTIGNORE)
(Hint): Consistency
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): What can be done when co-location is not possible?
(A): Abandon an Agile approach
(B): Have frequent digital meetings
(C): Supply necessary collaborative tools (e.g. SharePoint)
(D): Assume the work done by remote employees will be inferior and plan accordingly 
(Correct): B, C
(Points): 1
(CF): While more difficult, Agile without co-location is possible
(WF): Agile without co-location is possible and remote employees can be just as valuable
(STARTIGNORE)
(Hint):
(Subject): SDLC
(Difficulty): Beginner
(Applicability): General
(ENDIGNORE)


(Type): multiplechoice
(Category): 5
(Grade style): 1
(Random answers): 1
(Question): Which of the following mark the close phase?
(A): You run out of funds
(B): You run out of time
(C): All features complete
(D): Business value has been attained for the first time in the project
(Correct): A, B, C
(Points): 1
(CF): 
(WF): With Agile, business value early and often.
(STARTIGNORE)
(Hint): Agile attempts to provide value early and often
(Subject): SDLC
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)