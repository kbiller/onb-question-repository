(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Stephen Cerbone
(Course Site): Lynda
(Course Name): Up and Running with Eclipse
(Course URL): http://www.lynda.com/Eclipse-tutorials/Up-Running-Eclipse/111243-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What is the term used for a saved window setup within Eclipse?
(A): A perspective.
(B): A view.
(C): The window formatting.
(D): The view Configuration.
(E): The window setup.
(Correct): A
(Points): 1
(CF): The term used for a saved window setup within Eclipse is a perspective.
(WF): The term used for a saved window setup within Eclipse is a perspective.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The icons that show up in the "gutter" and displayed on certain folders are called "decorators".
(A): True
(B): False
(Correct): A
(Points): 1
(CF): The icons that show up in the "gutter" and are displayed on certain folders are called "decorators".
(WF): The icons that show up in the "gutter" and are displayed on certain folders are called "decorators".
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The term for "find and replace" in Eclipse is called
(A): finding.
(B): discovering.
(C): refactoring.
(D): replacing.
(E): searching.
(Correct): C
(Points): 1
(CF): The term for "find and replace" in Eclipse is called refactoring
(WF): The term for "find and replace" in Eclipse is called refactoring
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The gutter is a type of garbage system built into Eclipse.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The gutter is the column that icons appear in just to the left of the code.
(WF): The gutter is the column that icons appear in just to the left of the code.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): How do you set a breakpoint in Eclipse?
(A): Middle click a line.
(B): Double click the gutter.
(C): Right click context menu > Add Breakpoint.
(D): Middle click the gutter.
(E): Double click a line.
(Correct): B
(Points): 1
(CF): You set a breakpoint in Eclipse by double clicking the gutter.
(WF): You set a breakpoint in Eclipse by double clicking the gutter.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): The debugger is located at: Project > Debug As.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): The debugger is located at: Run > Debug As.
(WF): The debugger is located at: Run > Debug As.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): What function does "Step Over" perform when debugging?
(A): It skips the function or block of code.
(B): It performs the function or block of code.
(C): It moves line by line through the function or block of code.
(D): Nothing unless it comes across a class.
(E): It runs the rest of the scripts code.
(Correct): C
(Points): 1
(CF): The "Step Over" function walks through the code performing each line one by one as it is repeatedly called.
(WF): The "Step Over" function walks through the code performing each line one by one as it is repeatedly called.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): You can search an entire project by using search > file.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): You can search an entire project by using search > file.
(WF): You can search an entire project by using search > file.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Searching an entire project via the search > file method searches the back end java system, not plain text.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Searching an entire project via the search > file method searches in plain text.
(WF): Searching an entire project via the search > file method searches in plain text.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): truefalse
(Category): 0
(Grade style): 0
(Random answers): 1
(Question): Git is officially supported in the Eclipse IDE.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Git is officially supported in the Eclipse IDE.
(WF): Git is officially supported in the Eclipse IDE.
(STARTIGNORE)
(Hint):
(Subject): Up and Running with Eclipse
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)