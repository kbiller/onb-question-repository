(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
(Submitter): Kenneth Biller
(Course Site): Lynda
(Course Name): HTML5-Structure-Syntax-Semantics
(Course URL): https://www.lynda.com/HTML-tutorials/HTML5-Structure-Syntax-Semantics/182177-2.html
(Discipline): Technical
(ENDIGNORE)

(Type): truefalse
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): The old div and span tags are no longer used in HTML5.
(A): True
(B): False
(Correct): B
(Points): 1
(CF): Although there are many cases where new tags suc as nav and section can replace the div and span tags, they are still used in HTML5.
(WF): Although there are many cases where new tags suc as nav and section can replace the div and span tags, they are still used in HTML5.
(STARTIGNORE)
(Hint):
(Subject): HTML5-Structure-Syntax-Semantics
(Difficulty): Beginner
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): Which of the following are valid HTML5 content models (select all that apply)?
(A): Flow
(B): Block
(C): Sectioning
(D): Inline
(E): Heading
(Correct): A, C, E
(Points): 1
(CF): The Block and Section content models from HTML4 no longer apply in HTML5.  Flow, Sectioning and Heading are all valid for HTML5.
(WF): The Block and Section content models from HTML4 no longer apply in HTML5.  Flow, Sectioning and Heading are all valid for HTML5.
(STARTIGNORE)
(Hint):
(Subject): HTML5-Structure-Syntax-Semantics
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): The language the document is written in (eg: English, French) is specified by the lang attribute on which element?
(A): body
(B): head
(C): html
(D): header
(E): article
(Correct): C
(Points): 1
(CF): A language value (such as en for Englsh) may be specified on the html element.
(WF): A language value (such as en for Englsh) may be specified on the html element.
(STARTIGNORE)
(Hint):
(Subject): HTML5-Structure-Syntax-Semantics
(Difficulty): Intermediate
(Applicability): Course
(ENDIGNORE)


(Type): multiplechoice
(Category): 8
(Grade style): 0
(Random answers): 1
(Question): What is the name of the new tag in HTML5 that identifies a unit outside the domain document flow, that if removed does not affect the meaning of the document?
(A): refer
(B): external
(C): diagram
(D): chart
(E): figure
(Correct): E
(Points): 1
(CF): The figure tag identifies a unit, such as a chart or graph, that does not affect the document meaning or flow if removed.
(WF): The figure tag identifies a unit, such as a chart or graph, that does not affect the document meaning or flow if removed.
(STARTIGNORE)
(Hint):
(Subject): HTML5-Structure-Syntax-Semantics
(Difficulty): Intermediate
(Applicability): General
(ENDIGNORE)
