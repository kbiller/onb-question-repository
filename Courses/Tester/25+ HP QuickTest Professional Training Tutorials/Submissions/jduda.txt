(STARTIGNORE)

Note: Any text within the (STARTIGNORE) and (ENDIGNORE) tags will be ignored when importing this question template

INSTRUCTIONS
Fill in the blank question templates below then upload and check them under the "Tests -> Question bank -> Import questions" section.
Maximum of 50 questions can be uploaded at once

Your categories numbers are listed below for your reference to add to the (Category): line
For example, the line below will use the 'Generic' category

(Category):

Your categories:
0 Generic
1 Java
2 Axure
3 JIRA
4 OOP
5 SDLC
6 Communication
7 Unix Scripting
8 HTML
9 CSS
10 JavaScript
11 Angular JS
12 Design Patterns
13 Git
14 Java Design Pattern
15 Business Analysis
16 Version Control
17 Presentations
18 Software Testing
19 Foundations of Programming: Software Quality Assurance
20 PowerPoint 2013 Essential Training
21 SQL Essential Training
22 Web Design Fundamentals

(Grade style): Only applicable for 'multiplechoice' (with more then 1 correct answer) style questions
0: Give full score only when all correct responses are provided
1: Give partial score per correct response and deduct partial score for incorrect responses
2: Give partial score per correct response but do not deduct partial score for incorrect responses

(Submitter): This is the name of the person creating and submitting the questions.

(Course Site): This is the site the course was taken on. The options for this are Lynda, and Udemy.

(Course Name): This is the exact name of the course as it is written on the site.

(Course URL): The URL of the course.

(Discipline): This is what discipline the training is classified under, Professional or Technical.

(Hint): This is an optional field that can be used if you wish to provide a hint to the user about the question.

(Subject): This is similar to category but can be used to specify more specific detail on the category.

(Difficulty): This is a range of difficulty of the question, Beginner, Intermediate, and Advanced.

(Applicability): This is if the question is about the course or can be applied to the category in general. Course or General are accepted choices for this section.


(ENDIGNORE)

(STARTIGNORE)
Quiz Details
***Category may need to be changed/updated
(Submitter): Joseph Duda
(Course Site): Software Testing Help
(Course Name): 25+ HP QuickTest Professional Training Tutorials
(Course URL): http://www.softwaretestinghelp.com/qtp-tutorial-25-what-is-descriptive-programming/
(Discipline): Professional
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): A main focus of how UFT is used for testing is recording and playback of tests.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): One of UFT's main uses is to record a manual test and then play it back.
(WF): One of UFT's main uses is to record a manual test and then play it back.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What does AUT stand for?
(A): Automated Unified Test.
(B): Automatic User Test.
(C): Application Under Test.
(D): Application User Trials.
(E): None of the above.
(Correct): C
(Points): 1
(CF): AUT refers to Application Under Test, the application you are currently running your tests on.
(WF): AUT refers to Application Under Test, the application you are currently running your tests on.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): If you have a complex test with many actions and you want a visual of how they are sequenced, you would look at the ______.
(A): Test flow.
(B): Keyword view.
(C): Editor view.
(D): Object repository.
(E): Step generator.
(Correct): A
(Points): 1
(CF): Test flow shows a flowchart of how actions interact with each other.
(WF): Test flow shows a flowchart of how actions interact with each other.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): Which language are tests written in when using UFT?
(A): Java.
(B): C++.
(C): JavaScript.
(D): VBScript.
(E): Ruby.
(Correct): D
(Points): 1
(CF): UFT code is written using VBScript.
(WF): UFT code is written using VBScript.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): When you click the OK button in a program, a window should pop up that says "Hello <username>". What step should you add to make sure the test fails if the username does not match the current user?
(A): Standard step.
(B): Checkpoint step.
(C): Output value step.
(D): Comments.
(E): Specialized step.
(Correct): B
(Points): 1
(CF): A checkpoint step is used to ensure the actual value of an object matches the expected value.
(WF): A checkpoint step is used to ensure the actual value of an object matches the expected value.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): What are some of the things UFT can use to uniquely identify objects? (Choose 3)
(A): Mandatory properties.
(B): Secondary properties.
(C): Assistive properties.
(D): Ordinal identifier.
(E): Nominal identifier.
(Correct): A, C, D
(Points): 1
(CF): First, UFT will look at mandatory properties. If that is not enough to uniquely identify the object, it will add assistive properties one-by-one. If it is still not unique, an ordinal identifier will be added as well.
(WF): First, UFT will look at mandatory properties. If that is not enough to uniquely identify the object, it will add assistive properties one-by-one. If it is still not unique, an ordinal identifier will be added as well.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): You have an application which requires a user to put in their signature using the mouse. You want to record a test of the signature input to see if the application responds correctly when the signature goes out of bounds. What type of recording should you make?
(A): Normal (default).
(B): Low-level.
(C): Analog.
(D): Insight.
(E): UFT does not support that type of recording.
(Correct): C
(Points): 1
(CF): Analog recordings will record the exact mouse and keystrokes that a user performs. They are recorded as tracks and cannot be edited afterwards.
(WF): Analog recordings will record the exact mouse and keystrokes that a user performs. They are recorded as tracks and cannot be edited afterwards.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): VBScript only has one main data type.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Variant is the main data type of VBScript. Variant does contain more typical subtypes such as Boolean and Integer.
(WF): Variant is the main data type of VBScript. Variant does contain more typical subtypes such as Boolean and Integer.
(STARTIGNORE)
(Hint):
(Subject): QTP - VBScript
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): multiplechoice
(Category): 18
(Grade style): 0
(Random answers): 1
(Question): You want to create a checkpoint for a Windows application that ensures the company logo is positioned correctly relative to the application's title and the current user's image. Which checkpoint would be best for this situation?
(A): Standard checkpoint.
(B): Image Checkpoint.
(C): Bitmap checkpoint.
(D): Text checkpoint.
(E): Accessibility checkpoint.
(Correct): C
(Points): 1
(CF): A bitmap checkpoint is used to compare 'screenshots' of the AUT. Standard checkpoints check an object's property value. Image checkpoints are used only for web images.
(WF): A bitmap checkpoint is used to compare 'screenshots' of the AUT. Standard checkpoints check an object's property value. Image checkpoints are used only for web images.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)


(Type): truefalse
(Category): 18
(Grade style): 0
(Random answers): 0
(Question): If you want to run many iterations of a test with different input values, it would be more efficient to use a data table.
(A): True
(B): False
(Correct): A
(Points): 1
(CF): Data tables can be used for parametrization of test inputs. 
(WF): Data tables can be used for parametrization of test inputs.
(STARTIGNORE)
(Hint):
(Subject): QTP
(Difficulty): Beginner
(Applicability): course
(ENDIGNORE)